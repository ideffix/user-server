/*
 * Copyright (c) 2017 Nordic Consulting & Development Company.
 * All rights reserved.
 * NCDC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package pl.ncdc.userapp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.ncdc.userapp.model.User;

/**
 *
 * <p>
 * Created on 04.08.2017
 *
 * @author ideffix
 */
public interface UserRepository extends MongoRepository<User, String> {

	
	
}
