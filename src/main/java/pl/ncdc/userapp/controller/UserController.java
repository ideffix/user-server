/*
 * Copyright (c) 2017 Nordic Consulting & Development Company.
 * All rights reserved.
 * NCDC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package pl.ncdc.userapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.ncdc.userapp.model.User;
import pl.ncdc.userapp.repository.UserRepository;

/**
 *
 * <p>
 * Created on 04.08.2017
 *
 * @author ideffix
 */

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping("/add")
	public void addUser(@RequestBody User user) {
		userRepository.save(user);
	}
	
	@GetMapping("/all")
	public List<User> getUsers() {
		return userRepository.findAll();
	}

}
